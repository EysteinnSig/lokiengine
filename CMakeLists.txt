cmake_minimum_required(VERSION 3.0.0)
project(lokiengine VERSION 0.1.0)

include(CTest)
enable_testing()

add_library(imgui STATIC
    imgui/imgui.cpp
    #imgui/demo.cpp  
    imgui/imgui_draw.cpp
    imgui/imgui_widgets.cpp
    imgui/imgui_impl_glfw.cpp
    imgui/imgui_impl_opengl3.cpp)
target_include_directories(imgui PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/imgui})



add_executable(lokiengine 
#add_library(lokiengine
    src/main.cpp 
    src/graphics/window.cpp
    src/graphics/shader.cpp
    src/graphics/texture.cpp
    src/graphics/vertexbuffer.cpp
    src/graphics/window.cpp
    src/graphics/glfw_window.cpp
    src/bitmap/bitmap.cpp
    src/misc/system.cpp
    src/misc/rect.cpp)

target_compile_features(lokiengine PRIVATE cxx_std_17)

include_directories( src )

target_link_libraries(lokiengine imgui)

find_package(OpenGL REQUIRED)
find_package(GLEW REQUIRED)
include_directories( ${OPENGL_INCLUDE_DIRS} )
include_directories( ${GLEW_INCLUDE_DIRS} )

target_link_libraries(lokiengine ${OPENGL_LIBRARIES} )
target_link_libraries(lokiengine ${GLEW_LIBRARIES} )

# Add GLFW to project
set( GLFW_BUILD_DOCS OFF CACHE BOOL  "GLFW lib only" )
set( GLFW_INSTALL OFF CACHE BOOL  "GLFW lib only" )
add_subdirectory( glfw )
include_directories(glfw/include)
target_link_libraries(lokiengine glfw)

add_subdirectory(math2d)
target_link_libraries(lokiengine math2d)

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)
