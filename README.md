# lokiengine

A small 2D rendering engine to explore OpenGL programming


# Example of a texture and rotating sprite

```c++
using namespace Loki;
    
TexturepackerSheet sheet = TexturepackerSheet::load_file(System::get_content_path() / "examples/images/dungeon_0.json");

ImageAtlas atlas;
atlas.load_texturepacker(sheet);

auto img_staff = atlas.get_image("item/staff/staff_9.png");

Window *window = new GLFWWindow(500, 500, "Example: texture atlas & transformation");

window->view.resize(2,2);
atlas.gpu_load();

auto sprite = std::make_shared<Sprite>();
sprite->image = img_staff;

RenderBatch batch;
while (!window->should_close())
{
    auto window_size = window->get_window_size();
    glViewport(0, 0, window_size.x, window_size.y);
    
    window->clear(Color::DARKGRAY);

    float width = sprite->image->bitmap_frame.width();
    sprite->set_angle(System::get_time());        
    sprite->set_scale(Vec2f(1/width)+Vec2f(0.3*std::sin(System::get_time()))/width);

    batch.clear();
    batch.add(sprite);
    batch.draw(*window);

    window->display();
}
return 0;
```