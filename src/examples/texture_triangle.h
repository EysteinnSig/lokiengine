#include <iostream>
#include "graphics/opengl.h"
#include "graphics/window.h"
#include "graphics/glfw_window.h"
#include "misc/system.h"
#include "bitmap/bitmap.h"
#include "misc/imageatlas.h"
#include "misc/texturepacker.h"
#include "misc/system.h"
#include "math2d.h"
#include "misc/transform2d.h"
#include <map>


int example_texture_triangle()
{
    using namespace Loki;
    
    Loki::Window *window = new Loki::GLFWWindow(500, 500, "Example: textured triangle");
    window->view.resize(2,2);

    ImageAtlas atlas;
    auto img_ghost = atlas.load_imagefile(System::get_content_path() / "images/ghost.png");
    atlas.gpu_load();

    RenderState state;
    state.texture = img_ghost->texture.get();

    glEnable(GL_BLEND);
    glEnable(GL_TEXTURE_2D);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    while (!window->should_close())
    {
        auto window_size = window->get_window_size();
       
        glViewport(0, 0, window_size.x, window_size.y);
        window->view.resize(2,2);       
        window->clear(Color::DARKGRAY);

        std::vector<Loki::Vertex> vertices;
        
        vertices.push_back(Vertex(Vec2f(-0.5,-0.5), Color::WHITE, Vec2f(0, 0))); //image2->texture_frame.bottom_left() )); //(0, 0)));
        vertices.push_back(Vertex(Vec2f(-0.5, 0.5), Color::WHITE, Vec2f(0, 1))); //image2->texture_frame.top_left() )); //(0,1)));
        vertices.push_back(Vertex(Vec2f(0.5,  0.5), Color::WHITE, Vec2f(1, 1))); //image2->texture_frame.top_right() )); //Vec2f(1, 1)));

        vertices.push_back(Vertex(Vec2f(-0.5,-0.5), Color::WHITE, Vec2f(0, 0))); //image2->texture_frame.bottom_left() )); //(0, 0)));
        vertices.push_back(Vertex(Vec2f(0.5,  0.5), Color::WHITE, Vec2f(1, 1))); //image2->texture_frame.top_left() )); //(0,1)));
        vertices.push_back(Vertex(Vec2f(0.5, -0.5), Color::WHITE, Vec2f(1, 0))); //image2->texture_frame.top_right() )); //Vec2f(1, 1)));

        window->draw(vertices, state);
        window->display();
    }
    return 0;
}