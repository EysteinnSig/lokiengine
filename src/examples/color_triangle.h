#include <iostream>
#include "graphics/opengl.h"
#include "graphics/window.h"
#include "graphics/glfw_window.h"
#include "misc/system.h"
#include "bitmap/bitmap.h"

int example_color_triangle() {

    using namespace Loki;
    Loki::Window *window = new Loki::GLFWWindow(500, 500, "Example: color triangle");
    Texture tex = Texture::purple_checkerboard();
    
    while (!window->should_close())
    {
        auto window_size = window->get_window_size();
        glViewport(0, 0, window_size.x, window_size.y);

        glClearColor( 0.3, 0.3, 0.3, 1.0 );
        glClear(GL_COLOR_BUFFER_BIT);

        std::vector<Loki::Vertex> vertices;
        vertices.push_back(Vertex(Vec2f(-0.5,-0.5), Color::BLUE, Vec2f(0, 0)));
        vertices.push_back(Vertex(Vec2f(-0.5, 0.5), Color::RED, Vec2f(0,1)));
        vertices.push_back(Vertex(Vec2f(0.5,  0.5), Color::GREEN, Vec2f(1, 1)));
        
        window->draw(vertices);
        window->display();
    }
    return 0;
}
