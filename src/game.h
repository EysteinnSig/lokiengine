#include <iostream>
#include "graphics/opengl.h"
#include "graphics/window.h"
#include "graphics/glfw_window.h"
#include "misc/system.h"
#include "bitmap/bitmap.h"
#include "misc/imageatlas.h"
#include "misc/texturepacker.h"
#include "misc/system.h"
#include "math2d.h"
#include "misc/transform2d.h"
#include <map>
#include "misc/sprite.h"

namespace Loki {


int polygonize(std::vector<Vertex> &vertices, const std::shared_ptr<Image> image, const Transform2D &transform, const Color &color = Color::WHITE) // Vec3f pos, Vec2f size, float rot, Color color)
{
    const Mat3f &mat = transform.get_mat();
    float zlevel = transform.get_zlevel();
    float x, y;
    x = image->bitmap_frame.width()*0.5f;
    y = image->bitmap_frame.height()*0.5f;
    vertices.push_back(Vertex(mat*Vec2f(-x, -y), zlevel, color, image->texture_frame.top_left() )); //(0, 0)));
    vertices.push_back(Vertex(mat*Vec2f(-x, y), zlevel, color, image->texture_frame.bottom_left() )); //(0,1)));
    vertices.push_back(Vertex(mat*Vec2f(x,  y), zlevel, color, image->texture_frame.bottom_right() )); //Vec2f(1, 1)));

    vertices.push_back(Vertex(mat*Vec2f(-x, -y), zlevel, color, image->texture_frame.top_left() )); //(0, 0)));
    vertices.push_back(Vertex(mat*Vec2f(x,  y), zlevel, color, image->texture_frame.bottom_right() )); //Vec2f(1, 1)));
    vertices.push_back(Vertex(mat*Vec2f(x, -y), zlevel, color, image->texture_frame.top_right() )); //(0,1)));
    return 6;         
}



class RenderBatch
{
    
private:
    std::map<std::shared_ptr<Texture>, std::vector<Vertex>> batches;

public:
    int add(const std::shared_ptr<Image> image, const Transform2D &transform)
    {
        return polygonize(batches[image->texture], image, transform);
    }

    void clear() { batches.clear(); }

    void draw(Window &window)
    {
        RenderState state;
        for (auto &iter : this->batches)
        {
            auto &texture = iter.first;
            auto &vertices = iter.second;
            state.texture = texture.get();
            window.draw(vertices, state);
        }
    }
};

}

void move_sprite(std::shared_ptr<Loki::Sprite> sprite, Vec2f dpos)
{
    /*auto pos = sprite->transform.get_pos();
    pos = pos + dpos;
    sprite->transform.set_pos(pos);*/

    sprite->set_pos(sprite->get_pos()+dpos);
}
void clamp_sprite_pos(std::shared_ptr<Loki::Sprite> sprite, Vec2f min, Vec2f max)
{
    auto pos = sprite->get_pos();
    if (pos.x < min.x) pos.x = min.x;
    if (pos.y < min.y) pos.y = min.y;
    if (pos.x > max.x) pos.x = max.x;
    if (pos.y > max.y) pos.y = max.y;
    sprite->set_pos(pos);
}

class Entity;
struct Context {
    Loki::Window *window;
    Loki::ImageAtlas *atlas;
    std::vector<std::shared_ptr<Entity> > *entities;
};


class Entity : public std::enable_shared_from_this<Entity> {
public:
    std::shared_ptr<Loki::Sprite> sprite;
    Entity()
    {
        this->sprite = std::make_shared<Loki::Sprite>();
    }
    virtual void tick(double dt) 
    {

    }
    /*std::shared_ptr<Entity> getptr() {
        return shared_from_this();
    }*/
private:

};


class TreeEnemy : public Entity {
public:
    TreeEnemy(Context *context) //const Loki::ImageAtlas &atlas)
    {
        //accu = Loki::System::get_time();
        accu = Loki::System::random(-4, 4);
        printf("%f\n", accu);

        //sprite = std::make_shared<Loki::Sprite>();
        this->sprite->image = context->atlas->get_image("tree.png");
        this->sprite->set_pos(Vec2f(5,0)); 
        float width = sprite->image->bitmap_frame.width();
        
        this->sprite->set_scale(Vec2f(1.0f/width)); //, 1.0f/width));
        this->context = context;
        tick(0);
    }
    virtual void tick(double dt)
    {
        auto pos = this->sprite->get_pos();
        pos.x = pos.x - dt;
        pos.y = std::sin(pos.x)*2+accu;
        this->sprite->set_pos(pos);

        auto entities = context->entities;
        /*if (this->sprite->get_pos().x < -7)
            printf("Hallo");*/
        
    }
private:
    double accu = 0;
    Context *context;
};

class Bullet : public Entity {
public:
    Bullet(Context *context, const Vec2f &velocity)
    {
        this->velocity = velocity;
        this->sprite->image = context->atlas->get_image("bullet");
        //this->sprite->image
        this->context = context;
    }
    void tick(double dt)
    {
        auto pos = this->sprite->get_pos();
        pos = pos + this->velocity*dt;
        this->sprite->set_pos(pos);
        auto entities = context->entities;
        if (this->sprite->get_pos().x < -7)
            printf("Hallo");
        if (!context->window->view.intersects(sprite->get_AABB()))
            entities->erase(
                std::remove_if(entities->begin(), entities->end(), [this](const std::shared_ptr<Entity> &entity)
                {
                    return entity.get() == this;
                }),
                entities->end()
            );
            //this->context->entities.
    }
private:
    Vec2f velocity;
    Context *context;
};

class PlayerEntity : public Entity {
public:
    PlayerEntity(Context *context)
    {
        this->context = context;
        this->sprite->image = context->atlas->get_image("ghost.png");
        this->sprite->set_pos(Vec2f(-4, 0));
        float width = sprite->image->bitmap_frame.width();

        this->sprite->set_scale(Vec2f(1.0f/width)); //sprite->image->bitmap_frame.width(), 1.0f/sprite->image->bitmap_frame.height()));
    }
    void tick(double dt)
    {
        auto window_raw = ((Loki::GLFWWindow*)context->window)->get_raw();
        if (glfwGetKey(window_raw, GLFW_KEY_W) == GLFW_PRESS)
        {
            move_sprite(this->sprite, Vec2f(0, dt*6));
            clamp_sprite_pos(this->sprite, Vec2f(-4.5, -4.5), Vec2f(4.5, 4.5));
        }
        if (glfwGetKey(window_raw, GLFW_KEY_S) == GLFW_PRESS)
        {
            move_sprite(this->sprite, Vec2f(0, -dt*6));
            clamp_sprite_pos(this->sprite, Vec2f(-4.5, -4.5), Vec2f(4.5, 4.5));
        }
    }
private:
    Context *context;
};

int game()
{
    using namespace Loki;
    //using namespace Math2D;
    float pi = 3.14159265359;
    Vec2f v(2,0); //(0.7071067811865476,0.7071067811865476);
    Mat3f m = Mat3f::rotation(0.5*pi);
    Vec2f p = v*m;
    printf("%s\n", v.to_string().c_str());
    printf("%s\n", m.to_string().c_str());
    printf("%s\n\n", p.to_string().c_str());
    
    auto t1 = Mat3f::translation(Vec2f(2,0))*Mat3f::rotation(0.5*pi)*Mat3f::scaling(Vec2f(20,20));
    printf("%s\n", (t1*Vec2f(1)).to_string().c_str());

    //auto t2 = Mat3f::scaling(Vec2f(20,20)).rotate(0.5*pi)*translate(Vec2f(2,0))
    auto t2 = Mat3f::translation(Vec2f(2,0)).rotate(0.5*pi).scale(Vec2f(20,20));
    printf("%s\n", (t2*Vec2f(1)).to_string().c_str());
    
    


    //exit(0);
    

    const std::string json_name = "dungeon_0.json";
    auto json_text = System::read_ascii(json_name);
    
    ImageAtlas atlas;
    auto image1 = atlas.load_imagefile("dungeon_0.png");
    auto img_ghost = atlas.load_imagefile("ghost.png");
    auto img_tree = atlas.load_imagefile("tree.png");
    TexturepackerSheet sheet = TexturepackerSheet::parse_json(json_text);
    atlas.load_texturepacker(sheet);

    Loki::Window *window = new Loki::GLFWWindow(500, 500, "Example: texture atlas");
    window->view.resize(10,10);
    atlas.gpu_load();

    Context context;
    context.atlas = &atlas;
    context.window = window;

    RenderState state;

    glEnable(GL_BLEND);
    glEnable(GL_TEXTURE_2D);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    float rad = 0;

    RenderBatch batch;
    /*std::shared_ptr<Sprite> player_sprite = std::make_shared<Sprite>();
    player_sprite->set_pos(Vec2f(-4, 0));
    player_sprite->image = atlas.get_image("ghost.png");*/
    

    std::vector<std::shared_ptr<Entity>> entities;
    context.entities = &entities;

    std::shared_ptr<PlayerEntity> player_entity = std::make_shared<PlayerEntity>(&context);
    entities.push_back(player_entity);


    double prev_time;
    double curr_time = System::get_time();
    auto window_raw = ((GLFWWindow*)window)->get_raw();
    while (!window->should_close())
    {
        prev_time = curr_time;
        curr_time = System::get_time();
    
        //double curr_time = System::get_time();

        window->view.resize(10,10);

        auto window_size = window->get_window_size();
        glViewport(0, 0, window_size.x, window_size.y);
        
        window->clear(Color::DARKGRAY); //from_float(0.3f, 0.3f, 0.3f, 1.0f));

        std::vector<Loki::Vertex> vertices;
        /*Transform2D trans;
        trans.mat = Mat3f::identity()*Mat3f::rotation(rad);
        trans.zlevel = 0.1;*/
        
        //const Transform2D &trans = player_sprite->transform;

        batch.clear();
        /*{
            const Transform2D trans = *std::static_pointer_cast<Transform2D>(player_sprite);
            batch.add(player_sprite->image, trans);
        }*/
        double dt = curr_time - prev_time;
        /*if (System::random()<dt/2)
        {
            auto entity = std::make_shared<TreeEnemy>(&context);
            entities.push_back(entity);
        }*/

        View world_view = window->view;

        //if (!window->view.intersects(entity->sprite->get_AABB()))
        
        entities.erase(
            std::remove_if(entities.begin(), entities.end(), 
                [window](const std::shared_ptr<Entity> &entity)
                {
                    if (!window->view.intersects(entity->sprite->get_AABB()))
                    {
                        printf("Outside\n");
                        return false;
                    }
                    return false;
                    //return entity.get() == this;
                }),
            entities.end()
        );
        printf("Length: %i\n", entities.size());

        for (auto entity : entities)
        {
            

            entity->tick(dt);
            const Transform2D trans = *std::static_pointer_cast<Transform2D>(entity->sprite);
            batch.add(entity->sprite->image, trans);
        }
        batch.draw(*window);
        window->display();
        rad=System::get_time();
        
        if (glfwGetKey(window_raw, GLFW_KEY_A))
        {
            auto entity = std::make_shared<TreeEnemy>(&context);
            entities.push_back(entity);
        }
        if (glfwGetKey(window_raw, GLFW_KEY_ESCAPE))
        {
            break;
        }
        
        
    }
    return 0;
}