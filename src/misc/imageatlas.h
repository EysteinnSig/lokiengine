#pragma once

#include <vector>
#include <map>
#include "bitmap/bitmap.h"
#include "graphics/texture.h"
#include "misc/texturepacker.h"
#include "misc/logger.h"

namespace Loki {

struct Image {
    std::shared_ptr<Texture> texture;
    Rectf texture_frame;
    Recti bitmap_frame;
};


class ImageAtlas {
public:

    std::shared_ptr<Image> get_image(const std::string &filename) const
    {
        auto iter = images.find(filename);
        if (iter == images.end())
        {
            //printf("Warning: image (%s) not found\n");
            LOGE("Error reading file %s\n", filename.c_str());
            return nullptr;
        }
        return iter->second;
    }
    Recti flip_ud(const Recti &rect, int height)
    {
        Recti ret;
        ret.pos =  Vec2i(rect.pos.x, height - rect.top());
        ret.size = rect.size;
        return ret;
    }
    void load_texturepacker(const TexturepackerSheet &sheet)
    {
        std::shared_ptr<Bitmap> bitmap = std::make_shared<Bitmap>();
        auto sheet_dir = sheet.json_path.parent_path();
        bitmap->load_file(System::get_content_path() / sheet_dir / sheet.image_filename);
        bitmap->flip_ud();
        std::shared_ptr<Texture> texture = std::make_shared<Texture>();

        bitmaps_texture[bitmap] = texture;

        for (auto frame : sheet.frames)
        {
            std::shared_ptr<Image> image = std::make_shared<Image>();
            image->texture = texture;
            image->bitmap_frame = flip_ud(frame.rect, bitmap->height());
            //image->texture_frame = this->get_texture_rect(frame.rect, bitmap->width(), bitmap->height());
            image->texture_frame = this->get_texture_rect(image->bitmap_frame, bitmap->width(), bitmap->height());

            
            images[frame.filename] = image;
        }
    }

    std::shared_ptr<Image> load_bitmap(std::shared_ptr<Bitmap> bitmap, const std::string &filename)
    {
        std::shared_ptr<Texture> texture = std::make_shared<Texture>();
        bitmaps_texture[bitmap] = texture;
        
        std::shared_ptr<Image> image = std::make_shared<Image>();
        Recti frame(0,0, bitmap->width(), bitmap->height());
        image->bitmap_frame = frame;
        image->texture = texture;
        image->texture_frame = get_texture_rect(frame, bitmap->width(), bitmap->height());

        images[filename] = image;

        return image;
    }
    
    std::shared_ptr<Image> load_imagefile(const std::string &filename)
    {
        std::shared_ptr<Bitmap> bitmap = std::make_shared<Bitmap>();
        bitmap->load_file(System::get_content_path() / filename);
        bitmap->flip_ud();
        
        return load_bitmap(bitmap, filename);
    }

    void gpu_load()
    {
        for (auto &iter : bitmaps_texture)
        {
            auto bitmap = iter.first;
            auto texture = iter.second;
            texture->load(*bitmap);
        }
    }
private:

    Rectf get_texture_rect(const Recti &image_rect, float width, float height)
    {
        float x = image_rect.x() / width;
        //float y = (height - image_rect.bottom()) / width;
        float y = image_rect.y() / height;
        float w = image_rect.width() / width;
        float h = image_rect.height() / height;
        return Rectf(x, y, w, h); 
    }

    std::map<std::shared_ptr<Bitmap>, std::shared_ptr<Texture> > bitmaps_texture;
    std::map<std::string, std::shared_ptr<Image> > images;

};


}