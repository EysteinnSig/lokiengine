#pragma once

#include "math2d.h"

namespace Loki
{

class Transform2D
{
private:
    //Mat3f mat = Mat3f::identity();
    float zlevel = 0;
    float angle;
    Vec2f pos = Vec2f(0);
    Vec2f scale = Vec2f(1);
public:
    Transform2D() {}
    /*Transform2D(const Mat3f &mat_) : mat(mat_)
    {

    }
    Transform2D(const Mat3f &mat_, float zlevel_) : mat(mat_), zlevel(zlevel_)
    {

    } 
    Transform2D(float zlevel_) : zlevel(zlevel_)
    {

    }*/

    void set_zlevel(float zlevel) { this->zlevel = zlevel; }
    float get_zlevel() const { return this->zlevel; }

    void set_angle(float angle) { this->angle = angle; }
    float get_angle() const { return angle; }

    void set_pos(const Vec2f &pos) { this->pos = pos; }
    Vec2f get_pos() const { return this->pos; }

    void set_scale(const Vec2f &scale) { this->scale = scale; }
    Vec2f get_scale() const { return this->scale; }

    Mat3f get_mat() const 
    { 
        return Mat3f::translation(this->pos)*Mat3f::rotation(this->angle)*Mat3f::scaling(this->scale);
    }
};
    
} // namespace Loki
