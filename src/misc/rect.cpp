#include "rect.h"

namespace Loki {

namespace Utils {

Rectf get_texture_rect(const Recti &image_rect, float width, float height)
{
    float x = image_rect.x() / (float)width;
    float y = (height - image_rect.bottom()) / (float)width;
    float w = image_rect.width() / (float)width;
    float h = image_rect.height() / (float)height;
    return Rectf(x, y, w, h); 
}
    
}


}