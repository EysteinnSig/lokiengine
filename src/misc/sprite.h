#pragma once

namespace Loki {

class Sprite : public Transform2D
{
public:
    std::shared_ptr<Image> image;
    //Transform2D transform;

    Transform2D get_transform()
    {
        Transform2D *tf = (Transform2D*)this;
        return *tf;
        //return *std::static_pointer_cast<Transform2D>(this);
        //return std::static_pointer_cast<Transform2D>(this);
    }
    Rectf get_AABB()
    {
        
        //Transform2D *tf = (Transform2D*)this;
        float height = image->bitmap_frame.height();
        float width = image->bitmap_frame.width();
        
        Vec2f ul(-width/2, height/2);
        Vec2f ur(width/2, height/2);
        Vec2f lr(width/2, -height/2);
        Vec2f ll(-width/2, -height/2);
        

        const Mat3f &mat = this->get_mat();
        return Rectf::from_points({mat*ll, mat*ur, mat*lr, mat*ll}); 
        
    }


};

}