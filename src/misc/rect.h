#pragma once

#include <initializer_list>

#include "math/lokimath.h"
namespace Loki {


template<class T>
//struct __attribute__ ((__packed__)) Rect {
struct  Rect {
    

    Vec2<T> pos  = Vec2<T>(0, 0);
    Vec2<T> size = Vec2<T>(0, 0);   
    
    inline T width() const { return size.x; }
    inline T height() const { return size.y; }
    inline T x() const { return pos.x; }
    inline T y() const { return pos.y; }

    inline T top() const { return pos.y+size.y; } //+size.y; }
    //T bottom() const { return pos.y+size.y; }
    inline T bottom() const { return pos.y; }
    inline T left() const { return pos.x; }
    inline T right() const { return pos.x+size.x; }

    inline Vec2<T> get_center() const { return Vec2<T>(pos.x+size.x/2.0, pos.y+size.y/2.0); }
    inline Rect& set_center(const Vec2<T> &center) 
    { 
        pos.x = center.x-size.x/2.0;
        pos.y = center.y-size.y/2.0;
        return *this;
    }
    inline Rect& set_size(const Vec2<T> size)
    {
        this->size = size;
        return *this;
    }

    inline Vec2<T> top_left() const { return Vec2<T>(left(), top()); }
    inline Vec2<T> top_right() const { return Vec2<T>(right(), top()); }
    inline Vec2<T> bottom_left() const { return Vec2<T>(left(), bottom()); }
    inline Vec2<T> bottom_right() const { return Vec2<T>(right(), bottom()); }
    
    inline std::string to_string() const
    {
        return "Rect (x: " + std::to_string(pos.x) + ", y: "+ std::to_string(pos.y)+", width: "+std::to_string(size.x)+", height: "+std::to_string(size.y) + ")";
    }

    Rect<T>()
    {
    }

    bool intersects(const Rect<T> &other)
    {
        /*printf("%s Top: %f  Bottom: %f \t %s Top: %f  Bottom: %f\n", 
            this->to_string().c_str(), this->top(), this->bottom(), other.to_string().c_str(), other.top(), other.bottom());*/
        if (
            other.bottom() > this->top() ||
            other.top() < this->bottom() ||
            other.left() > this->right() ||
            other.right() < this->left()
            )
            return false;
        return true;
    }
    /*Rect<T> contain(const Rect<T> &other)
    {
        printf("%f   %f\n", this->y_sign, other.y_sign);
        assert(this->y_sign == other.y_sign);
        T left = std::min(this->left(), other.left());
        T right = std::max(this->right(), other.right());
        T bottom = std::min(this->bottom(), other.bottom());
        T top = std::max(this->top(), other.top());

        auto ret = Rect<T>::from_corners(Vec2<T>(left, bottom), Vec2<T>(right, top), this->is_y_flipped());
        printf("%f\n", ret.y_sign);
        return ret;
    } */

    /*
    *  @brief Creates a rectangle object
    *  @param  x  Left side.
    *  @param  y  Upper side.
    *  @param  width  Width of the rectangle.
    *  @param  height  Height of the rectangle.
    *  @param  flip_y  Is the y-axis flipped with positive downward? For pixel (bitmap) coordinate system use true, for OpenGL use false
    *  @return   Returns instance of rectangle.
    *
    *  Creates rectangle item
    */
    Rect<T>(T x, T y, T width, T height) : Rect<T>()
    {
        pos.x = x;
        pos.y = y;
        size.x = width;
        size.y = height;
    }

    inline static Rect<T> from_corners(const Vec2<T> &lower_left, const Vec2<T> &upper_right)
    {
        return Rect<T>(lower_left.x, lower_left.y, 
            std::abs(lower_left.x-upper_right.x), std::abs(lower_left.y-upper_right.y));
    }
    inline static Rect<T> from_center(const Vec2<T> &center, const Vec2<T> &size)
    {
        return Rect<T>(center.x - size.x/2, center.y - size.y/2, size.x, size.y);
    }
    inline static Rect<T> from_points(const std::initializer_list<Vec2<T> > &points)
    {
        
        T left=0, right=0, top=0, bottom=0;

        for (auto iter = points.begin(); iter != points.end(); iter++)
        {
            if (iter == points.begin())
            {
                left = (*iter).x;
                right = (*iter).x;
                top = (*iter).y;
                bottom = (*iter).y;
            } else
            {
                left = std::min(left, (*iter).x);
                right = std::max(right, (*iter).x);
                bottom = std::min(bottom, (*iter).y);
                top = std::max(top, (*iter).y);
            }
        }
        return Rect<T>::from_corners(Vec2<T>(left, bottom), Vec2<T>(right, top));
    }

    
    template<class Archive>
    void serialize(Archive &ar)
    {
        ar(pos, size);
    }
};

using Rectf = Rect<float>;
using Recti = Rect<int>;

namespace Utils {
Rectf get_texture_rect(const Recti &image_rect, float width, float height);
}

/*void test_rect()
{
    Rectf r1(-1, 0, 2, 2, false);
    assert(r1.top() == 0);
    assert(r1.bottom() == -2);
    assert(r1.get_center().x == 0);
    assert(r1.get_center().y == -1);

    Rectf r2(-1, 0, 2, 2, true);
    assert(r2.top() == 0);
    assert(r2.bottom() == 2);
    assert(r2.get_center().y == 1);

    
}*/
 
}