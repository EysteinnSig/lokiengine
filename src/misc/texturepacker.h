#pragma once

#include <string>
//#include "misc/image.h"
#include "misc/json.hpp"
#include "math/lokimath.h"
#include "misc/rect.h"

namespace Loki {
    
struct TexturePackerFrame
{   
    Recti rect;
    bool rotated;
    bool trimmed;
    std::string filename;
};

class TexturepackerSheet
{
public:
    TexturepackerSheet() {}

    TexturepackerSheet(TexturepackerSheet &&o) : frames(std::move(o.frames)) 
    {
        image_filename = o.image_filename;
        image_size = o.image_size;
    }
    
    


    static TexturepackerSheet load_file(const std::string &json_file)
    {
        auto json_text = System::read_ascii(System::get_content_path() / json_file);
        
        TexturepackerSheet sheet = TexturepackerSheet::parse_json(json_text);
        sheet.json_path = json_file;
        return sheet;

    }

    std::vector<TexturePackerFrame> frames;
    std::string image_filename;
    Vec2i image_size;
    std::filesystem::path json_path;

private:
    static TexturepackerSheet parse_json(const std::string &jsontext)
    {
        using json = nlohmann::json;
        json j = json::parse(jsontext);

        TexturepackerSheet sheet;

        json jmeta = j["meta"];
        sheet.image_filename = jmeta["image"];
        int bitmap_width = jmeta["size"]["w"].get<int>();
        int bitmap_height = jmeta["size"]["h"].get<int>();
        sheet.image_size = Vec2i(bitmap_width, bitmap_height);
        for (json &jframe : j["frames"])
        {
            Recti image_rect;
            image_rect.pos.x = jframe["frame"]["x"].get<int>();
            image_rect.pos.y = jframe["frame"]["y"].get<int>();
            image_rect.size.x = jframe["frame"]["h"].get<int>();  
            image_rect.size.y = jframe["frame"]["w"].get<int>();


            TexturePackerFrame tpframe;
            tpframe.rect = image_rect;
            tpframe.filename = jframe["filename"].get<std::string>();
            tpframe.rotated = jframe["rotated"].get<bool>();
            tpframe.trimmed = jframe["trimmed"].get<bool>();

            sheet.frames.push_back(tpframe);
        }
        return sheet;
    }
    
};


/*TexturepackerSheet parse_texturepacker(const std::string &jsontext) //, std::string &bitmap_name, std::vector<Image> &image_infos)
{
    using json = nlohmann::json;
    json j = json::parse(jsontext);

    TexturepackerSheet sheet;

    json jmeta = j["meta"];
    sheet.image_filename = jmeta["image"];
    int bitmap_width = jmeta["size"]["w"].get<int>();
    int bitmap_height = jmeta["size"]["h"].get<int>();
    sheet.image_size = Vec2i(bitmap_width, bitmap_height);

    for (json &jframe : j["frames"])
    {
        Recti image_rect;
        image_rect.pos.x = jframe["frame"]["x"].get<int>();
        image_rect.pos.y = jframe["frame"]["y"].get<int>();
        image_rect.size.x = jframe["frame"]["h"].get<int>();  // <-- w? h? Suspicious
        image_rect.size.y = jframe["frame"]["w"].get<int>();


        TexturePackerFrame tpframe;
        tpframe.rect = image_rect;
        tpframe.filename = jframe["filename"].get<std::string>();
        tpframe.rotated = jframe["rotated"].get<bool>();
        tpframe.trimmed = jframe["trimmed"].get<bool>();

        sheet.frames.push_back(tpframe);
    }
}*/

} // namespace Loki
