
#ifndef SRC_MISC_SYSTEM_H_
#define SRC_MISC_SYSTEM_H_

#pragma once


#if defined(_WIN32) || defined(_WIN64)
#define LOKI_WINDOWS
#elif defined(__linux__)
#define LOKI_LINUX
#elif defined(__ANDROID__)
#define LOKI_ANDROID
#else
#error Platform not supported
#endif

#include <limits.h>
#include <filesystem>
#include <vector>
#ifdef LOKI_LINUX
#include <unistd.h>
#endif
#include <fstream>
#include <random>
#include "misc/logger.h"



namespace Loki {

namespace System {

inline float pi = 3.14159265358979f;

double random(double min=0, double max=1);

double get_time();

std::filesystem::path get_exec_path();

std::filesystem::path get_save_path();

std::filesystem::path get_content_path(); //const std::filesystem::path &relpath = "");

std::vector<uint8_t> read_binary(const std::string &filename);

std::vector<uint8_t> read_binary(const std::filesystem::path &filename);

std::string read_ascii(const std::string &filename);

std::string read_ascii(const std::filesystem::path &filename);


}
}

#endif