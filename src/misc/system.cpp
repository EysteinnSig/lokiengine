#include "system.h"
#include <chrono>
namespace Loki {
namespace System {


double random(double min, double max) { 


    static std::default_random_engine generator;
    //std::mt19937 generator(std::random_device());
    std::uniform_real_distribution<double> distribution(min, max);

    return distribution(generator);

    /*static std::uniform_real_distribution<double> *dist = nullptr;
    if (!dist)
    {

        std::random_device rd;
        std::mt19937 mt(rd());
        dist = new std::uniform_real_distribution<double>(0.0, 1.0); 
    }
    return dist(*mt);*/
}

double get_time()
{
    //auto now = std::chrono::system_clock::now();
    auto now = std::chrono::steady_clock::now();
    //auto dur = std::chrono::steady_clock::duration();

    //std::chrono::duration<double> dur = now - 0;// std::chrono::steady_clock::duration();
    //return dur.count();
    /*auto dur = now.time_since_epoch();    
    //std::chrono::duration<double> dur = std::chrono::steady_clock::now(); //.time_since_epoch();
    //return dur.count();
    //std::chrono::duration<double>(dur);
    //return std::chrono::duration_cast<std::chrono::seconds>(dur).count();
    //return dur.count() * std::chrono::system_clock::period::num / std::chrono::system_clock::period::den;
    std::chrono::duration<double> d = dur;
    
    return d.count();*/

    std::chrono::duration<double> duration = now.time_since_epoch();
    return duration.count();
}

std::filesystem::path get_exec_path()
{
    std::filesystem::path ret;
#if defined(LOKI_LINUX)
    char result[ PATH_MAX ];
    ssize_t count = readlink( "/proc/self/exe", result, PATH_MAX );
    //return std::string( result, (count > 0) ? count : 0 );
    ret = std::filesystem::path(std::string( result, (count > 0) ? count : 0 ));
#elif defined(LOKI_WINDOWS)
#error Not implemented
#elif defined(LOKI_ANDROID)
#error Not implemented
#else 
#error System not defined 
#endif
    return ret;
}
std::filesystem::path get_save_path()
{
    auto exec_path = get_exec_path().parent_path();
    auto save_path = std::filesystem::canonical(exec_path / "../") / "saves" ;
    if (!std::filesystem::exists(save_path))
    {
        printf("Save directory does not exist, creating one.\n");
        std::filesystem::create_directory(save_path);
        printf("Done creating save directory.\n");
    }

    return std::filesystem::canonical(exec_path / "../content/");
    
}
std::filesystem::path get_content_path() //const std::filesystem::path &relpath)
{
    auto exec_path = get_exec_path().parent_path();
    //printf("Exec path: %s\n", exec_path.c_str());
    return std::filesystem::canonical(exec_path / "../content/"); // / relpath);
}





std::vector<uint8_t> read_binary(const std::string &filename)
{
    std::vector<uint8_t> bytes;
#if defined(LOKI_ANDROID)
    ANativeActivity *activity_ = static_app->activity;

    AAssetManager* assetManager = activity_->assetManager;
    AAsset* assetFile = AAssetManager_open( assetManager, filename.c_str(), AASSET_MODE_BUFFER );
    if( !assetFile )
    {
        LOGI("Error opening file: '%s'", filename.c_str());
        //pthread_mutex_unlock( &mutex_ );
        return false;
    }

    size_t size = AAsset_getLength( assetFile );
    bytes.clear();
    //bytes.reserve(size);
    bytes.resize(size, 0);


    int bytesRead = AAsset_read(assetFile, &bytes[0], size);
    AAsset_close(assetFile);
    if (bytesRead < 0)
    {
        LOGI( "Failed to load:%s", filename.c_str() );

    }
    return bytes;
//#elif __IPHONE_OS_VERSION_MIN_REQUIRED

#elif defined(LOKI_LINUX)
    std::streampos fileSize;
    std::ifstream file(filename, std::ios::binary);
    if (!file.is_open())
    {
        LOGE("Error reading binary file: %s\n", filename.c_str());
        return bytes;
    }
    // get its size:
    file.seekg(0, std::ios::end);
    fileSize = file.tellg();
    file.seekg(0, std::ios::beg);


    bytes.resize(fileSize);
    if (fileSize>0)
        file.read((char*) &bytes[0], fileSize);
    return bytes;
#else
#error Error
#endif
}

std::vector<uint8_t> read_binary(const std::filesystem::path &filename)
{
    return read_binary(filename.string());
}


std::string read_ascii(const std::string &filename)
{
    auto vec = read_binary(filename);
    return std::string(vec.begin(), vec.end());
}

std::string read_ascii(const std::filesystem::path &filename)
{
    return read_ascii(filename.string());
}


}
}