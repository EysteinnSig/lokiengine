#pragma once

#include <map>
#include <memory>
#include "graphics/texture.h"
#include "misc/imageatlas.h"
#include "misc/sprite.h"

namespace Loki
{
    


int polygonize(std::vector<Vertex> &vertices, const std::shared_ptr<Image> image, const Transform2D &transform, const Color &color = Color::WHITE) // Vec3f pos, Vec2f size, float rot, Color color)
{
    const Mat3f &mat = transform.get_mat();
    float zlevel = transform.get_zlevel();
    float width = image->bitmap_frame.width()*0.5f;
    float height = image->bitmap_frame.height()*0.5f;
    auto &texframe = image->texture_frame;
    vertices.push_back(Vertex(mat*Vec2f(-width/2,-height/2), Color::WHITE, texframe.bottom_left())); // (0, 0))); //image2->texture_frame.bottom_left() )); //(0, 0)));
    vertices.push_back(Vertex(mat*Vec2f(-width/2, height/2), Color::WHITE, texframe.top_left())); // Vec2f(0, 1))); //image2->texture_frame.top_left() )); //(0,1)));
    vertices.push_back(Vertex(mat*Vec2f(width/2,  height/2), Color::WHITE, texframe.top_right())); //Vec2f(1, 1))); //image2->texture_frame.top_right() )); //Vec2f(1, 1)));

    vertices.push_back(Vertex(mat*Vec2f(-width/2,-height/2), Color::WHITE, texframe.bottom_left())); // Vec2f(0, 0))); //image2->texture_frame.bottom_left() )); //(0, 0)));
    vertices.push_back(Vertex(mat*Vec2f(width/2,  height/2), Color::WHITE, texframe.top_right())); // Vec2f(1, 1))); //image2->texture_frame.top_left() )); //(0,1)));
    vertices.push_back(Vertex(mat*Vec2f(width/2, -height/2), Color::WHITE, texframe.bottom_right())); // Vec2f(1, 0))); //image2->texture_frame.top_right() )); //Vec2f(1, 1)));

    /*
    float x, y;
    x = image->bitmap_frame.width()*0.5f;
    y = image->bitmap_frame.height()*0.5f;
    vertices.push_back(Vertex(mat*Vec2f(-x, -y), zlevel, color, image->texture_frame.top_left() )); //(0, 0)));
    vertices.push_back(Vertex(mat*Vec2f(-x, y), zlevel, color, image->texture_frame.bottom_left() )); //(0,1)));
    vertices.push_back(Vertex(mat*Vec2f(x,  y), zlevel, color, image->texture_frame.bottom_right() )); //Vec2f(1, 1)));

    vertices.push_back(Vertex(mat*Vec2f(-x, -y), zlevel, color, image->texture_frame.top_left() )); //(0, 0)));
    vertices.push_back(Vertex(mat*Vec2f(x,  y), zlevel, color, image->texture_frame.bottom_right() )); //Vec2f(1, 1)));
    vertices.push_back(Vertex(mat*Vec2f(x, -y), zlevel, color, image->texture_frame.top_right() )); //(0,1)));*/

    return 6;         
}

class RenderBatch
{
    
private:
    std::map<std::shared_ptr<Texture>, std::vector<Vertex>> batches;

public:
    int add(const std::shared_ptr<Image> &image, const Transform2D &transform)
    {
        return polygonize(batches[image->texture], image, transform);
    }

    int add(const std::shared_ptr<Sprite> &sprite)
    {
        //return polygonize(batches[])
        return add(sprite->image, sprite->get_transform());
    }

    void clear() { batches.clear(); }

    void draw(Window &window)
    {
        RenderState state;
        for (auto &iter : this->batches)
        {
            auto &texture = iter.first;
            auto &vertices = iter.second;
            state.texture = texture.get();
            window.draw(vertices, state);
        }
    }
};


} // namespace Loki
