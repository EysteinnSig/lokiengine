
#ifndef SRC_MISC_BITMAP_H_
#define SRC_MISC_BITMAP_H_

#include <filesystem>
#include <vector>
#include <cstdint>

#include "color.h"


namespace Loki {

class Bitmap
{
private:
    int w, h, c;
    std::vector<std::uint8_t> d;

    inline size_t get_index(int x, int y)
    {
        return c*(x + y*w);
    }
public:

    inline const std::vector<std::uint8_t> &bytes() const { return d; }
    inline int height() const { return h; }
    inline int width() const { return w; }
    

    void load_buffer(const std::vector<std::uint8_t> &buffer);
    void load_file(const std::filesystem::path &path);

    void set_pixel(int x, int y, const Color &color);
    Color get_pixel(int x, int y);
    Bitmap& flip_ud();
    Bitmap();

    Bitmap(int width, int height);

    Bitmap(const std::filesystem::path &filename);

    static const Bitmap &purple_checkerboard()
    {
        static Bitmap *phold = nullptr;
        if (!phold)
        {
            phold = new Bitmap(8,8);
            Color magenta(248, 0, 248);
            Color black(0, 0, 0);
            for (int y=0; y<8; y++)
            {
                for (int x=0; x<8; x++)
                {
                    Color col = black;
                    if ((x+y) % 2)
                        col = magenta;
                    phold->set_pixel(x, y, col);
                }
            }  
        }
        return *phold;
    }

    static const Bitmap &white_checkerboard()
    {
        static Bitmap *phold = nullptr;
        if (!phold)
        {
            phold = new Bitmap(8,8);
            //Color magenta(248, 0, 248);
            Color white(255);
            Color black(0);
            for (int y=0; y<8; y++)
            {
                for (int x=0; x<8; x++)
                {
                    Color col = black;
                    if ((x+y) % 2)
                        col = white;
                    phold->set_pixel(x, y, col);
                }
            }  
        }
        return *phold;
    }

};

}

/*void test_bitmap()
{
    Loki::Bitmap bitmap;
    bitmap.load_file("/home/eysteinn/projects/cpp/magestower/content/images/main/dungeon_0.png");

}*/

#endif
