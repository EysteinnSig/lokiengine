#include "bitmap.h"
#include "misc/system.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

namespace Loki {

void Bitmap::load_buffer(const std::vector<std::uint8_t> &buffer)
{
    int x,y, channels;
    int desired_channels = STBI_rgb_alpha;
    
    stbi_uc *bitmapbuffer = stbi_load_from_memory(buffer.data(), buffer.size(), &x, &y, &channels, desired_channels);
    //printf("Image read: %i x %i x %i\n", x, y, channels);
    assert(channels == desired_channels);
    assert(bitmapbuffer);
    size_t size = x*y*channels;
    d.assign(bitmapbuffer, bitmapbuffer+size);
    w = x;
    h = y;
    c = channels;
    stbi_image_free(bitmapbuffer);
    //printf("%i %i %i\n", x, y, channels);
}
void Bitmap::load_file(const std::filesystem::path &path)
{
    std::vector<std::uint8_t> buffer = Loki::System::read_binary(path);
    load_buffer(buffer);
}

void Bitmap::set_pixel(int x, int y, const Color &color)
{
    size_t index = get_index(x,y);
    d[index] = color.r;
    d[index+1] = color.g;
    d[index+2] = color.b;
    d[index+3] = color.a;
    
}
Color Bitmap::get_pixel(int x, int y)
{
    Color color;
    size_t index = get_index(x,y);
    color.r = d[index]; 
    color.g = d[index+1]; 
    color.b = d[index+2]; 
    color.a = d[index+3];
    return color;
}
Bitmap& Bitmap::flip_ud()
{
    int line_bytes = this->w*this->c;
    std::uint8_t buffer[line_bytes];
    
    for (int y=0; y<this->h/2; y++)
    {
        int top_index = y*this->w*this->c;
        int bottom_index = (this->h-y-1)*this->w*this->c;
        memcpy(buffer, &this->d[top_index], line_bytes);  // copy upper line to buffer
        memcpy(&d[top_index], &d[bottom_index], line_bytes); // copy lower line to upper line
        memcpy(&d[bottom_index], buffer, line_bytes); // copy buffer to lower line
    }
    return *this;
}


Bitmap::Bitmap() {}

Bitmap::Bitmap(int width, int height)
{
    this->w = width; 
    this->h = height;
    this->c = 4;
    this->d.assign(width*height*c, 255);
}

Bitmap::Bitmap(const std::filesystem::path &filename)
{
    this->load_file(filename);
}


}