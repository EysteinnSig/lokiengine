

#ifndef SRC_MISC_COLOR_H_
#define SRC_MISC_COLOR_H_

#pragma once

#include <cstdint>

namespace Loki {


struct __attribute__ ((__packed__)) Color;

struct __attribute__ ((__packed__)) Color 
{
    //union {
    std::uint8_t r, g, b, a;
    //std::uint8_t bytes[4];
    //};

    Color(std::uint8_t c) : r(c), g(c), b(c), a(255)
    {

    }
    Color() : r(255), g(255), b(255), a(255)
    {

    }
    Color(std::uint8_t r_, std::uint8_t g_, std::uint8_t b_, std::uint8_t a_=255): r(r_), g(g_), b(b_), a(a_)
    {

    }

    static Color from_float(float r, float g, float b, float a)
    {
        return Color(r*255, g*255, b*255, a*255);
    }


    static const Color BLACK, WHITE, RED, BLUE, GREEN, CYAN, DARKGRAY, LIGHTGRAY;
    

    /*static const int q = 1;
    static const Color WHITE = Color(255, 255, 255, 255);
    static inline Color RED = Color(255, 255, 255, 255);
    static constexpr Color BLUE;*/
    

    
};

inline const Color Color::BLACK( 0, 0, 0, 255 );
inline const Color Color::WHITE( 255, 255, 255, 255 );
inline const Color Color::RED( 255, 0, 0, 255 );
inline const Color Color::BLUE( 0, 0, 255, 255 );
inline const Color Color::GREEN( 0, 128, 0, 255 );
inline const Color Color::CYAN( 0, 255, 255, 255 );
inline const Color Color::DARKGRAY(169, 169, 169, 255);
inline const Color Color::LIGHTGRAY( 211, 211, 211, 255);

}
#endif