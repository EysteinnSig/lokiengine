#include "shader.h"

namespace Loki
{

std::string Shader::vertsrc = 
    "#version 330 core\n"
    "uniform mat2 transform;"
    "uniform vec2 origin;"
    //"layout (location = 0) in vec2 aPos; // the position variable has attribute position 0\n"
    "layout (location = 0) in vec3 aPos; // the position variable has attribute position 0\n"
    "layout (location = 1) in vec4 aColor;\n"
    "layout (location = 2) in vec2 aTexCoord;\n"
    "out vec4 vertexColor; // specify a color output to the fragment shader\n"
    "out vec2 vertexTexCoord;"
    "void main()\n"
    "{\n"
    //"    gl_Position = vec4((aPos-origin)*transform, 0.0, 1.0); // see how we directly give a vec3 to vec4's constructor\n"
    "    gl_Position = vec4((aPos.xy-origin)*transform, aPos.z, 1.0); // see how we directly give a vec3 to vec4's constructor\n"
    "    vertexColor = aColor; // set the output variable to a dark-red color\n"
    "    vertexTexCoord = aTexCoord;"
    "}\n";
std::string Shader::fragsrc = 
    "#version 330 core\n"
    "out vec4 FragColor;\n"
    "in vec4 vertexColor; // the input variable from the vertex shader (same name and same type)\n"
    "in vec2 vertexTexCoord;"
    "uniform sampler2D ourTexture;"
    "void main()\n"
    "{\n"
    //"    FragColor = vertexColor;\n"
    //"  if (vertexColor.r > 1000) FragColor = vec"
    //"  FragColor = vec4(vertexTexCoord.x, 0, 0, 1);"
    "   vec4 texcolor = texture2D(ourTexture, vertexTexCoord);"// + vertexColor;"
    //"   FragColor = (FragColor+vertexColor)*0.5f;"
    //"   FragColor.rgba = vec4(mix(texcolor.rgb, vertexColor.rgb, 0.5),1);"
    "   FragColor = texcolor*vertexColor;"
    //"   FragColor = texcolor;"
    //"   FragColor = vertexColor;"
    "}\n";

GLuint Shader::compile_shader(GLenum shaderType, const std::string& shaderSource)
{
    assert(shaderType == GL_VERTEX_SHADER || shaderType == GL_FRAGMENT_SHADER);
    GLuint shader = 0;
    shader = glCreateShader(shaderType);
    if (shader) {
        const char *c_str = shaderSource.c_str();
        glShaderSource(shader, 1, &c_str, NULL);
        glCompileShader(shader);                
        GLint compiled = 0;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
        if (!compiled) {
            GLint infoLen = 0;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
            if (infoLen) {
                //char* buf = (char*) malloc(infoLen);
                char* buf = new char[infoLen];
                if (buf) {
                    glGetShaderInfoLog(shader, infoLen, NULL, buf);
                    //LOGD("Could not compile shader %d:\n%s\n", shaderType, buf);
                    printf("Could not compile %s shader:\n%s\n", shaderType == GL_VERTEX_SHADER ? "vertex" : "fragment", buf);

                    //free(buf);
                    delete[] buf;
                }
                glDeleteShader(shader);
                shader = 0;
            }
        }
    }
    return shader;
}

GLuint Shader::create_link_program(GLuint vert_handle, GLuint frag_handle)
{
    GLuint prog_handle = glCreateProgram();
    if (prog_handle != 0)
    {
        glBindAttribLocation(prog_handle, 4, "position");
        glAttachShader(prog_handle, vert_handle);
        glAttachShader(prog_handle, frag_handle);
        //glBindAttribLocation(prog_handle, 4, "InVertex");
        glLinkProgram(prog_handle);
        GLint linkstatus = 0;
        glGetProgramiv(prog_handle,GL_LINK_STATUS, &linkstatus);
        if (!linkstatus)
        {
            printf("Could not link shader program.\n");
            glDeleteProgram(prog_handle);
            prog_handle = 0;
        }
    }
    return prog_handle;
}

void Shader::read_info()
{
    //printf("Reading info\n");
    GLint numActiveAttribs = 0;
    GLint numActiveUniforms = 0;
    glGetProgramiv(this->program_handle, GL_ACTIVE_ATTRIBUTES, &numActiveAttribs);
    glGetProgramiv(this->program_handle, GL_ACTIVE_UNIFORMS, &numActiveUniforms);
    //printf("Attribs: %i    Uniforms: %i\n", (int)numActiveAttribs, (int)numActiveUniforms);
    attributes.clear();
    uniforms.clear();
    int buffSize = 255;
    GLchar name[buffSize];
    int offset = 0;
    for (GLint k=0; k<numActiveAttribs; k++)
    {
        GLsizei length;
        Attribute attrib;
        glGetActiveAttrib(this->program_handle, k, buffSize-1, &length, &attrib.size, &attrib.type, &name[0]);
        attrib.location = glGetAttribLocation(this->program_handle,  &name[0]);
        GLsizei byteSize = 0;
        GLint componentCount;
        GLenum componentType = GL_FLOAT;
        switch (attrib.type)
        {
            case GL_FLOAT:
                byteSize = sizeof(GLfloat);
                componentCount = 1;
                break;
            case GL_FLOAT_VEC2:
                byteSize = sizeof(GLfloat)*2;
                componentCount = 2;
                break;
            case GL_FLOAT_VEC3:
                byteSize = sizeof(GLfloat)*3;
                componentCount = 3;
                break;
            case GL_FLOAT_VEC4:
                byteSize = sizeof(GLfloat)*4;
                componentCount = 4;
                break;
            case GL_FLOAT_MAT2:
                byteSize = sizeof(GLfloat)*2*2;
                componentCount = 4;
                break;
            case GL_FLOAT_MAT3:
                byteSize = sizeof(GLfloat)*3*3;
                componentCount = 4;
                break;
            case GL_FLOAT_MAT4:
                byteSize = sizeof(GLfloat)*4*4;
                componentCount = 4;
                break;
            default:
                printf("Attribute type not recognised\n");
                assert(false);
        }
        attrib.componentCount = componentCount;
        attrib.componentType = componentType;
        attrib.byteSize = byteSize;
        attrib.index = k;
        attrib.name = std::string(name, length);
        attrib.offset = offset;
        //attrib.stride = 20;
        this->attributes[std::string(name, length)] = attrib;

        offset += byteSize;
    }
    for (auto &iter : attributes)
    {
        Attribute &attrib = iter.second;
        attrib.stride = offset;
    }
//  http://www.opengl.org/wiki/Common_Mistakes#Texture_Unit
//  int MaxTextureImageUnits;
//  glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &MaxTextureImageUnits);





                    
}

GLuint Shader::get_handle()  const { return this->program_handle; }
void Shader::set_uniform(const std::string &name, const Texture &texture)
{
    GLint loc =  glGetUniformLocation(this->program_handle, name.c_str());
}
void Shader::set_uniform(const std::string &name, const Mat2f &mat2)
{
    GLint loc =  glGetUniformLocation(this->program_handle, name.c_str());
    glUniformMatrix2fv(loc, 1, false, mat2.data);
    
}
void Shader::set_uniform(const std::string &name, const Vec2f &vec2)
{
    GLint loc =  glGetUniformLocation(this->program_handle, name.c_str());
    float v[2];
    v[0] = vec2.x; v[1] = vec2.y;
    glUniform2fv(loc, 1, v);
}
bool Shader::is_valid()
{
    return this->program_handle != 0 && glIsProgram(this->program_handle);
}
void Shader::bind() const
{
    glUseProgram(this->program_handle);
}

void Shader::load_source(const std::string &vertsrc, const std::string &fragsrc)
{
    //printf("Vert shader: %s\n", this->vertsrc.c_str());
    GLuint verthandle = this->compile_shader(GL_VERTEX_SHADER, this->vertsrc);
    GLuint fraghandle = this->compile_shader(GL_FRAGMENT_SHADER, this->fragsrc);
    program_handle = this->create_link_program(verthandle, fraghandle);
    this->read_info();
}
Shader::Shader()
{
    
    //printf("%i - %i\n", (int)verthandle, (int)fraghandle);
}


Shader& Shader::default_shader()
{
    static Shader shader;
    if (!shader.is_valid())
    {
        shader.load_source(shader.vertsrc, shader.fragsrc);
        
    }
    return shader;
}    
    
} // namespace Loki

