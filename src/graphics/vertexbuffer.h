#pragma once

#include <memory>
#include "opengl.h"
#include "graphics/vertex.h"


namespace Loki {

class VertexBuffer
{
private:
    //std::vector<Vertex> vertices;
    GLuint vbo_handle=0;
    //GLuint vao_handle=0;
    std::shared_ptr<GLuint> vao_handle;
    
    //std::shared_ptr<GLuint> handle;
        //GLuint handle = 0;
    void delete_buffer(GLuint *vao_handle);
    /*void delete_buffer()
    {
        if (*vao_handle != 0)
            glDeleteVertexArrays(1, vao_handle.get());
        if (vbo_handle != 0)
            glDeleteBuffers(1, &vbo_handle);

        printf("Deleting VAO: %i\n", (int)*vao_handle);
        *vao_handle = 0;
        vbo_handle = 0;
        
    }*/
    void generate_buffer();
public:
    inline GLuint get_raw_vbo_handle() { return vbo_handle; }
    inline GLuint get_raw_vao_handle() { return *vao_handle; }

    size_t vertex_count = 0;
    //void get_refcnt() { printf("%i\n", (int)this->handle.use_count()); }
    //std::vector<Vertex> vertices;
    //std::shared_ptr<GLuint> get_handle() { return handle; }
    inline bool is_valid() { return *vao_handle != 0 && glIsBuffer(*vao_handle); }
    void load(const std::vector<Vertex> &vertices);
    void bind() const;

    GLint read_vertex_count();
    

    VertexBuffer();
     
};

}