#pragma once

#include <cassert>

//#include "misc/system.h"
#include "opengl.h"
#include "vertex.h"
#include "vertexbuffer.h"
//#include "bitmap/color.h"
#include "shader.h"
#include "texture.h"

/*#include "graphics/shapes.h"
#include "graphics/sprite.h"
#include "misc/imageatlas.h"

#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include "imgui.h"*/


namespace Loki {
struct RenderState
{
    Shader *shader = nullptr;
    const Texture *texture = nullptr;
};

struct View : public Loki::Rectf
{
    View(); // : Loki::Rectf(-1, 1, 2, 2, false);

    void zoom(float v);

    void resize(float w, float h);

    void set_width(float w, Vec2i window_size);

    void set_height(float h, Vec2i window_size);
    /* 
    Clip space has (-1, 1) in upper left corner and (1, -1) in lower right corner.
    Transforms to world.
    */
    Vec2f clip2world(const Vec2f &clip);
    
    template <class Archive>
    void serialize( Archive & ar );
};

class Window
{
private:
    Window(const Window&) = delete;
    Window& operator=(const Window&) = delete;

protected:
    Window() { }
    //virtual void init_window(int width, int height, const std::string &title="") = 0;
public:
    View view;
    
    
    //void init_imgui();
    //void draw_gui();
    static void error_callback(int error, const char* description);
    
    //void new_gui_frame();
    
#if defined(LOKI_CONTEXT_GLFW)

#endif
    
public:

#if defined(LOKI_CONTEXT_GLFW)
    //GLFWwindow *get_raw() const;
#endif

    //Window(int width, int height, const std::string &title="");
    virtual void clear(const Color &bgcolor = Color::BLACK) = 0;

    virtual bool should_close() const = 0;

    /* Screen space (0,0) in upper left corner, (width, height) in lower right corner
    */
    Vec2f screen2clip(const Vec2f &screen_coords);

    Vec2f screen2world(const Vec2f &screen_coords);
    virtual void display() = 0;
    virtual void set_window_title(const std::string &title) = 0;
    virtual void set_window_size(const Vec2i &size) = 0;

    virtual Vec2i get_window_size() = 0;
    void get_gl_version(int &major, int &minor);
    std::string get_gl_version_string();

    void draw(const VertexBuffer &buffer, const RenderState &renderstate);

    void draw(const std::vector<Vertex> &vertices, const RenderState &renderstate = RenderState());
};


/*void test_window()
{
    Window window(300, 300, "This is title");
    while(true)
    {
        window.display();
    }
}

void test_window_vertices()
{
    Window window(300, 300, "Test");
    VertexBuffer vbuffer;
    std::vector<Vertex> vertices;
    vertices.push_back(Vertex());
    vertices.push_back(Vertex());
    vbuffer.load(vertices);
    printf("Size: %i\n", (int)vbuffer.read_vertex_count());
}
void test_shaders()
{
    Window window(300, 300, "Test");
    
    test_shader1();

}
void test_render()
{
    int screen_size=800;
    Window window(screen_size, screen_size, "This is title");
    window.init_imgui();

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    

    //Shader shader = Shader::default_shader();
    
    float size = 1;
    Color c1(255, 0, 0, 255), c2(0, 255, 0, 255);
    

    

    RenderState renderstate;
    //renderstate.shader = &shader;
    VertexBuffer vertexbuffer;
    

    CheckOpenGLError();
    ImageAtlas sprite_atlas;
    sprite_atlas.load_file(System::get_content_path() / "images" / "main" / "dungeon_0.json");
    

    Bitmap bitmap(1,1); bitmap.set_pixel(0, 0, Color(255, 128, 0, 255));
    bitmap.load_file("/home/eysteinn/projects/cpp/tower/content/images/main/dungeon_0.png");
    bitmap.flip_ud();
    
    
    //shader.set_uniform("ourTexture", texture);  CheckOpenGLError();
    renderstate.texture = &sprite_atlas.get_texture(sprite_atlas["wall/undead_brown_1.png"].texture_idx);
    //renderstate.texture = &texture;
    Recti image_rect(1, 1, 32, 32);
    //RectShape::draw(vertices, Mat2f::scaling(Vec2f(30, 30)), Color(255, 255, 255, 255), texture.get_texture_rect(image_rect));
    //RectShape::draw(vertices, Mat2f::scaling(Vec2f(0.75, 0.75)), Color(255, 255, 255, 255), texture.get_texture_rect(image_rect)); //Rectf(0, 0, 1, 1));
    
    
    //RectShape::draw(vertices, sprite.get_transform(), Color(255, 255, 255, 255), sprite.texture.get_texture_rect(image_rect)); //Rectf(0, 0, 1, 1));

    
    //sprite.image_rect = sprite_atlas["gateways/hive_portal.png"].image_rect;
    
    //ImGui_ImplGlfw_NewFrame();
    while(true)
    {
        std::vector<Vertex> vertices;
        vertices.push_back(Vertex(Vec2f(-size, -size), c1, Vec2f(0, 0)));
        vertices.push_back(Vertex(Vec2f(-size, size), c1, Vec2f(0, 1)));
        vertices.push_back(Vertex(Vec2f(size, -size), c1, Vec2f(1, 0))); 
        vertices.push_back(Vertex(Vec2f(size, size), c1, Vec2f(1, 1)));
        vertices.push_back(Vertex(Vec2f(size, -size), c1, Vec2f(1, 0)));
        vertices.push_back(Vertex(Vec2f(-size, size), c1, Vec2f(0, 1))); 

        //sprite.rotate(-Loki::System::pi*0.01);

        //printf("%f\n", sprite.get_rotation());
        
        //sprite.draw(vertices);
        vertexbuffer.load(vertices);

        glClearColor( 0.3, 0.3, 0.3, 1.0 );
        glClear(GL_COLOR_BUFFER_BIT);

        //ImGui_ImplGlfw_NewFrame();
        window.new_gui_frame();
        
        ImGui::NewFrame();

        // 1. Show the big demo window (Most of the sample code is in ImGui::ShowDemoWindow()! You can browse its code to learn more about Dear ImGui!).
        //if (show_demo_window)
            //ImGui::ShowDemoWindow(&show_demo_window);
        ImGui::Begin("Demo window", nullptr, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoBackground);
        if (ImGui::Button("Hello!", ImVec2(100,40)))
            printf("Clicked\n");
        ImGui::End();

        {
            static float f = 0.0f;
            static int counter = 0;

            ImGui::Begin("Hello, world!");                          // Create a window called "Hello, world!" and append into it.

            ImGui::Text("This is some useful text.");               // Display some text (you can use a format strings too)
            //ImGui::Checkbox("Demo Window", &show_demo_window);      // Edit bools storing our window open/close state
            //ImGui::Checkbox("Another Window", &show_another_window);

            ImGui::SliderFloat("float", &f, 0.0f, 1.0f);            // Edit 1 float using a slider from 0.0f to 1.0f
            //ImGui::ColorEdit3("clear color", (float*)&clear_color); // Edit 3 floats representing a color

            if (ImGui::Button("Button"))                            // Buttons return true when clicked (most widgets return true when edited/activated)
                counter++;
            ImGui::SameLine();
            ImGui::Text("counter = %d", counter);

            ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
            ImGui::End();
        }


        // Rendering
        
        //glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
        //glClear(GL_COLOR_BUFFER_BIT);
        window.draw(vertexbuffer, renderstate); CheckOpenGLError();
        
        window.draw_gui();
        //ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());



        window.display();  CheckOpenGLError();
        
        //printf("Rendering\n");
    }
}
void windowed_tests()
{
    //test_window();
    test_render();
}*/

}