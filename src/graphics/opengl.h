
#pragma once
#include <cassert>
#include <cstdio>
#include "platform.h"

//#include "misc/system.h"

#if defined(LOKI_LINUX) || defined(LOKI_WINDOWS)
#define LOKI_CONTEXT_GLFW
#endif

#if defined(LOKI_CONTEXT_GLFW)
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#endif



#ifdef NDEBUG
#define CheckOpenGLError() ((void)0)
#else
#define CheckOpenGLError() _CheckOpenGLError(__FILE__, __LINE__)
#endif

static inline void ClearGLError() { GLenum glErr; while ((glErr = glGetError()) != GL_NO_ERROR); }
static inline void _CheckOpenGLError(const char *file, int line)
{
    GLenum glErr;
    while ((glErr = glGetError()) != GL_NO_ERROR)
    {
        printf("glError in file %s @ line %d code: 0x%X\n", file, line, glErr);
        //exit(1);
    };
}

