#pragma once
#include "opengl.h"
#include "window.h"
#include "math/lokimath.h"

namespace Loki {

class GLFWWindow : public Window
{
private:
    

    static void window_size_callback(GLFWwindow *glfwwindow, int screen_width, int screen_height);
    GLFWwindow *window = nullptr;
    void init_window(int width, int height, const std::string &title);

public:
    GLFWWindow(int width, int height, const std::string &title);
    GLFWwindow *get_raw() const;

    bool is_fullscreen()
    {
        return glfwGetWindowMonitor(this->get_raw());
    }
    void set_fullscreen(bool fullscreen)
    {
        auto winp = this->get_raw();
        if (fullscreen)
        {
            GLFWmonitor* monitor = glfwGetPrimaryMonitor();
            const GLFWvidmode* mode = glfwGetVideoMode(monitor);
            //auto winp = ((GLFWWindow*)window)->get_raw();
            //glfwGetWindowPos(winp, &x, &y);
            //glfwGetWindowSize(winp, &width, &height);
            glfwSetWindowMonitor(winp, monitor, 0, 0, mode->width, mode->height, mode->refreshRate);
        }
        else
        {
            auto size = get_window_size();
            
            glfwSetWindowMonitor(winp, nullptr, 0, 0, size.x / 2, size.y / 2, 0);
        }
    }
    virtual void clear(const Color &bgcolor = Color::BLACK);
    virtual bool should_close() const;
    virtual void set_window_title(const std::string &title);
    virtual void set_window_size(const Vec2i &size);
    virtual Vec2i get_window_size();
    virtual void display();
};

}