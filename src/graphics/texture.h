#pragma once

#include <cassert>
#include <memory>
#include "opengl.h"
//#include "bitmap/bitmap.h"
#include "misc/rect.h"

namespace Loki {

class Bitmap;

class Texture
{
public:
    //GLuint handle = 0;
    std::shared_ptr<GLuint> handle = 0;
    GLuint get_handle();
    void destroy_handle(GLuint *handle);
    /*void destroy()
    {
        if (handle && *handle != 0)
        {
            printf("Destroying texture: %i\n", (int)*handle);
            glDeleteTextures(1, handle.get());
            *handle = 0;
            handle=nullptr;
            
        }
    }*/
    void create();
    int width, height;
public:
    
    bool is_valid() const;

    GLuint get_handle() const;

    int get_width() const;
    int get_height() const;
    Rectf get_texture_rect(const Recti &image_rect);
    void bind() const;

    void load(const Bitmap &bitmap);
    
    static Texture &empty_texture();
    static Texture &purple_checkerboard();
    static Texture &white_checkerboard();
    
    Texture();

    Texture(const Bitmap &bitmap);
};

/*void test_texture()
{
    Bitmap bitmap;
    bitmap.load_file("/home/eysteinn/projects/cpp/magestower/content/images/main/dungeon_0.png");
    Texture texture;
    texture.load(bitmap);
    

}*/


}