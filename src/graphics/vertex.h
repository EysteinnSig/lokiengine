#pragma once

#include <vector>
#include "math/lokimath.h"
#include "bitmap/color.h"

namespace Loki {

struct __attribute__ ((__packed__)) Vertex
//struct Vertex
{
    Vec3f pos;
    Color color;
    Vec2f texcoord;
    
    Vertex() {}
    Vertex(const Vec2f &pos_, float zlevel, const Color &color_, const Vec2f &texcoord_)
    {
        pos = Vec3f(pos_.x, pos_.y, zlevel);
        color = color_;
        texcoord = texcoord_;
    }
    
    Vertex(const Vec2f &pos_, const Color &color_, const Vec2f &texcoord_)
    {
        pos = Vec3f(pos_.x, pos_.y, 0);
        //pos = pos_;
        color = color_;
        texcoord = texcoord_;
    }
    
    Vertex(const Vec2f &pos_, const Color &color_)
    {
        pos = Vec3f(pos_.x, pos_.y, 0);
        //pos = pos_;
        color = color_;
    }
    Vertex(const Vec3f &pos, const Color &color, const Vec2f &texcoord)
    {
        this->pos = pos;
        this->color = color;
        this->texcoord = texcoord;
    }
    Vertex(const Vec3f &pos, const Vec2f &texcoord)
    {
        this->pos = pos;
        this->texcoord = texcoord;
        this->color = Color::WHITE;
    }
};

/*
void test_vertex()
{
    struct VertexTest {
        union U {
            struct somename{
                Vec2f pos;
                int color;
                
            };
            char l[sizeof(float)+sizeof(int)];
            U() { }
            
        };
    };
    VertexTest vt;
   
}*/


}