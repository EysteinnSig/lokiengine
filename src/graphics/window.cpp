#include "window.h"


namespace Loki {

View::View() : Loki::Rectf(-1, -1, 2, 2)
{

}

void View::zoom(float v)
{
    auto center = this->get_center();
    this->size = size*v;
    this->set_center(center);
}

void View::resize(float w, float h)
{
    auto center = this->get_center();
    this->set_size(Vec2f(w, h));
    this->set_center(center);
}

void View::set_width(float w, Vec2i window_size)
{
    auto center = get_center();
    float aspect_ratio = window_size.y/(float)window_size.x;
    resize(w, w*aspect_ratio);
}

void View::set_height(float h, Vec2i window_size)
{ 
    auto center = get_center();
    float aspect_ratio = window_size.x/(float)window_size.y;
    resize(h*aspect_ratio, h);
}
/* 
Clip space has (-1, 1) in upper left corner and (1, -1) in lower right corner.
Transforms to world.
*/
Vec2f View::clip2world(const Vec2f &clip)
{
    Vec2f world = clip/2.0*Mat2f::scaling(this->size) + this->get_center();    
    //Vec2f world = this->bottom_left()+(clip+1)/2*this->size;
    return world;
}

/*template <class Archive>
void View::serialize( Archive & ar )
{ 
    // We pass this cast to the base type for each base type we
    // need to serialize.  Do this instead of calling serialize functions
    // directly
    ar( cereal::base_class<Rectf>( this )); 
}*/



/*
void Window::init_imgui()
{
    const char* glsl_version = "#version 130";
    IMGUI_CHECKVERSION();

    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); //(void)io;
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
    
    // Setup Dear ImGui style
    //ImGui::StyleColorsDark();
    ImGui::StyleColorsClassic();

    // Setup Platform/Renderer bindings
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);
    ImGui_ImplOpenGL3_NewFrame();

    ImGui_ImplGlfw_NewFrame();
    //ImGui::NewFrame();
    new_gui_frame();
}
void Window::draw_gui()
{
    
    ImGui::Render();
    int display_w, display_h;
    glfwGetFramebufferSize(window, &display_w, &display_h);
    glViewport(0, 0, display_w, display_h);
    //glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
    //glClear(GL_COLOR_BUFFER_BIT);
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

}*/
void Window::error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}



/*
void Window::new_gui_frame()
{
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
}
*/
//#if defined(LOKI_CONTEXT_GLFW)


/* Screen space (0,0) in upper left corner, (width, height) in lower right corner
*/
Vec2f Window::screen2clip(const Vec2f &screen_coords)
{
    Vec2i size = this->get_window_size();
    
    Vec2f ret;
    ret.x = 2*screen_coords.x / size.x -1;
    ret.y = 2*(1.0-screen_coords.y / size.y) -1;

    return ret;
}

Vec2f Window::screen2world(const Vec2f &screen_coords)
{
    auto clip = this->screen2clip(screen_coords);
    auto world = this->view.clip2world(clip);
    return world;
}
void Window::get_gl_version(int &major, int &minor)
{
    glGetIntegerv(GL_MAJOR_VERSION, &major);
    glGetIntegerv(GL_MINOR_VERSION, &minor);
}
std::string Window::get_gl_version_string()
{
    return std::string((char*)glGetString(GL_VERSION));
}

void Window::draw(const VertexBuffer &buffer, const RenderState &renderstate)
{
    
    GLenum err;
    
    if (buffer.vertex_count == 0)
        return;
        
    Shader *shader = renderstate.shader; CheckOpenGLError();
    const Texture *texture = renderstate.texture; CheckOpenGLError();
    if (shader == nullptr)
        shader = &(Shader::default_shader());
    if (texture == nullptr)
        texture = &(Texture::empty_texture());

    assert(shader);
    assert(texture);
    assert(texture->is_valid());   CheckOpenGLError();
    
    shader->bind();  CheckOpenGLError();
    buffer.bind();  CheckOpenGLError();
    shader->set_uniform("ourTexture", *texture);  CheckOpenGLError();
    //shader->set_uniform("uView", Loki::Mat2f(1, 0, 0, 1));
    //shader->set_uniform("origin", Loki::Vec2f(-1, -1));  CheckOpenGLError();
    /*float tmp = 20;
    Rectf rect(-tmp/2, -tmp/2, tmp, tmp);*/
    auto rect = this->view;
    auto transform = Mat2f::scaling(Vec2f(2/rect.width(), 2/rect.height()));
    //rect.set_center(Vec2f(-1, -1));
    auto origin = rect.get_center();


    shader->set_uniform("origin", origin);  CheckOpenGLError();
    //shader->set_uniform("transform", Loki::Mat2f::scaling(Vec2f(0.5,1)));  CheckOpenGLError();
    //shader->set_uniform("transform", Loki::Mat2f::rotation(45/2.0f/3.14f));  CheckOpenGLError();
    shader->set_uniform("transform", transform);  CheckOpenGLError();

    //shader->set_uniform("transform", Loki::Mat2f::identity());  CheckOpenGLError();

    GLint texUnitLoc = glGetUniformLocation(shader->get_handle(), "ourTexture");  CheckOpenGLError();
    glProgramUniform1i(shader->get_handle(), texUnitLoc , 0);  CheckOpenGLError();
    glActiveTexture(GL_TEXTURE0 ); CheckOpenGLError();
    texture->bind();  CheckOpenGLError();
    



    glDrawArrays(GL_TRIANGLES, 0, (GLsizei)buffer.vertex_count);  CheckOpenGLError();


    //render_gui();
    //printf("Done loop\n");
}

void Window::draw(const std::vector<Vertex> &vertices, const RenderState &renderstate)
{
    static VertexBuffer vb;
    vb.load(vertices);
    
    this->draw(vb, renderstate);
}


}