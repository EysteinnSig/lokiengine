#pragma once

#include <string>
#include <cassert>
#include <map>
#include "opengl.h"
#include "texture.h"
#include "math/lokimath.h"

namespace Loki
{


class Shader
{
public:
    struct Attribute
    {

            GLenum type;
            GLint size;
            GLint index;
            GLboolean normalized = GL_FALSE;
            GLsizei stride = 0;
            //GLvoid* ptr = NULL;;
            int offset = 0;
            bool enabled = true;
            GLsizei byteSize;

            GLint componentCount;
            GLenum componentType;
            GLint location = -2;
            std::string name = "";
    };
    std::map<std::string, Attribute> attributes;
    //std::vector<Attribute> attributelist;

    struct Uniform
    {
            GLenum type;
            GLint size;

            GLint location = -2;

    };
    std::map<std::string, Uniform> uniforms;

    GLuint program_handle = 0;
private:
    static std::string vertsrc;
    static std::string fragsrc;
    GLuint compile_shader(GLenum shaderType, const std::string& shaderSource);

    GLuint create_link_program(GLuint vert_handle, GLuint frag_handle);

    void read_info();
    
public:
    GLuint get_handle() const;
    void set_uniform(const std::string &name, const Texture &texture);
    void set_uniform(const std::string &name, const Mat2f &mat2);
    void set_uniform(const std::string &name, const Vec2f &vec2);
    bool is_valid();
    void bind() const;

    void load_source(const std::string &vertsrc, const std::string &fragsrc);
    Shader();
    

    static Shader &default_shader();

};

/*void test_shader1()
{
    Shader shader;
    
}*/

}