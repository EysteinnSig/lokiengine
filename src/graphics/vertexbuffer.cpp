#include <cassert>
#include "vertexbuffer.h"


namespace Loki {

void VertexBuffer::delete_buffer(GLuint *vao_handle)
{
    if (vao_handle && *vao_handle != 0)
    {
        //printf("Destroying vao handle: %i and vbo handle: %i\n", (int)*vao_handle, (int)vbo_handle);
        assert(glIsVertexArray(*vao_handle));
        glDeleteVertexArrays(1, vao_handle);
        glDeleteBuffers(1, &vbo_handle);
        assert(!glIsVertexArray(*vao_handle));
        assert(!glIsBuffer(vbo_handle));
    
        //printf("Delete => VBO: %i\t VAO: %i\n", vbo_handle, *vao_handle);

        *vao_handle = 0;
        vbo_handle = 0;
    }

}
/*void delete_buffer()
{
    if (*vao_handle != 0)
        glDeleteVertexArrays(1, vao_handle.get());
    if (vbo_handle != 0)
        glDeleteBuffers(1, &vbo_handle);

    printf("Deleting VAO: %i\n", (int)*vao_handle);
    *vao_handle = 0;
    vbo_handle = 0;
    
}*/
void VertexBuffer::generate_buffer()
{
    if (*vao_handle != 0)
        return;


    
    glGenVertexArrays(1, vao_handle.get()); CheckOpenGLError();
    glBindVertexArray(*vao_handle); CheckOpenGLError();
    assert(glIsVertexArray(*vao_handle));

    glGenBuffers(1, &vbo_handle);  CheckOpenGLError(); 
    glBindBuffer(GL_ARRAY_BUFFER, vbo_handle);  CheckOpenGLError();
    assert(glIsBuffer(vbo_handle));

    //printf("Generating VAO: %i and VBO: %i\n", (int)*vao_handle, (int)vbo_handle);
    // position
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), ((char *)NULL)+0); CheckOpenGLError();
    glEnableVertexAttribArray(0); CheckOpenGLError();

    // color
    glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Vertex), ((char *)NULL)+sizeof(Vec3f)); CheckOpenGLError();
    glEnableVertexAttribArray(1); CheckOpenGLError();

    // tex_coord
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), ((char *)NULL)+sizeof(Vec3f)+sizeof(Color));
    glEnableVertexAttribArray(2);

    glBindVertexArray(0);

    //printf("Created => VBO: %i\t VAO: %i\n", vbo_handle, *vao_handle);
}

void VertexBuffer::load(const std::vector<Vertex> &vertices)
{
    if (vertices.empty())
        return;

    this->generate_buffer();

    glBindBuffer(GL_ARRAY_BUFFER, vbo_handle);
    //glBindBuffer(GL_ARRAY_BUFFER, *handle);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex)*vertices.size(), &vertices[0], GL_STREAM_DRAW);
    vertex_count = vertices.size();
    
}
void VertexBuffer::bind() const
{
    glBindVertexArray(*vao_handle);
}

GLint VertexBuffer::read_vertex_count()
{
    this->bind();
    GLint size = 0;
    glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &size);
    return size / sizeof(Vertex);
}


VertexBuffer::VertexBuffer()
{
    //this->handle = std::shared_ptr<GLuint>(new GLuint(0), [this](GLuint *h){ this->delete_buffer(); });
    this->vao_handle = std::shared_ptr<GLuint>(new GLuint(0), [this](GLuint *h){ this->delete_buffer(h); });
    
}



}