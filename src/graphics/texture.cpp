#include "texture.h"
#include "graphics/opengl.h"
#include "bitmap/bitmap.h"
#include "misc/rect.h"


namespace Loki 
{

GLuint Texture::get_handle() { return *handle; }


void Texture::destroy_handle(GLuint *handle)
{
    
    if (handle && *handle != 0)
    {
        //printf("Destroying handle: %i\n", (int)*handle);
        glDeleteTextures(1, handle);
        *handle = 0;
    }
}
/*void destroy()
{
    if (handle && *handle != 0)
    {
        printf("Destroying texture: %i\n", (int)*handle);
        glDeleteTextures(1, handle.get());
        *handle = 0;
        handle=nullptr;
        
    }
}*/
void Texture::create()
{
    if (handle && *handle == 0)
    {
        //glGenTextures(1, &handle);
        glGenTextures(1, handle.get());
        //printf("Generated texture: %i\n", (int)*handle);
    }

}
bool Texture::is_valid() const { assert(handle); return *handle != 0 && glIsTexture(*handle); }

GLuint Texture::get_handle() const { return *handle; }

int Texture::get_width() const { return width; }
int Texture::get_height() const { return height; }
Rectf Texture::get_texture_rect(const Recti &image_rect)
{
    return Utils::get_texture_rect(image_rect, this->get_width(), this->get_height());
}
void Texture::bind() const
{
    //glBindTexture(GL_TEXTURE_2D, handle);
    glBindTexture(GL_TEXTURE_2D, *handle);
}

void Texture::load(const Bitmap &bitmap)
{
    create();
    assert(handle && *handle!=0);
    glBindTexture(GL_TEXTURE_2D, *handle);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); //GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); //GL_LINEAR);
    const std::vector<std::uint8_t> &data = bitmap.bytes();
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, bitmap.width(), bitmap.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, bitmap.bytes().data());
    this->width = bitmap.width();
    this->height = bitmap.height();
}

Texture &Texture::empty_texture()
{
    static Texture texture;
    if (!texture.is_valid())
    {
        Bitmap bitmap(2,2);
        texture.load(bitmap);
        
        assert(texture.is_valid());
    }
    return texture;
}

Texture &Texture::purple_checkerboard()
{
    static Texture texture;
    if (!texture.is_valid())
    {
        const Bitmap &bitmap = Bitmap::purple_checkerboard();
        texture.load(bitmap);
        assert(texture.is_valid());
    }
    return texture;
}

Texture &Texture::white_checkerboard()
{
    static Texture texture;
    if (!texture.is_valid())
    {
        const Bitmap &bitmap = Bitmap::white_checkerboard();
        texture.load(bitmap);
        assert(texture.is_valid());
    }
    return texture;
}
Texture::Texture() 
{
    //this->handle = std::shared_ptr<GLuint>(new GLuint(0), [this](GLuint *h){ this->destroy(); });
    this->handle = std::shared_ptr<GLuint>(new GLuint(0), [this](GLuint *h){ this->destroy_handle(h); });
    //printf("Init handle: %i\n", (int)*handle);
}

Texture::Texture(const Bitmap &bitmap) : Texture()
{
    this->load(bitmap);
}

} // Loki