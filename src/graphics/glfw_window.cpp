#include "glfw_window.h"

namespace Loki {

void GLFWWindow::window_size_callback(GLFWwindow *glfwwindow, int screen_width, int screen_height)
{
    Window *window = (Window*)glfwGetWindowUserPointer(glfwwindow);
    auto width = window->view.width();
    window->view.set_width(width, Vec2i(screen_width, screen_height));
}

GLFWWindow::GLFWWindow(int width, int height, const std::string &title)
{
    glfwSetErrorCallback(error_callback);
    if (!glfwInit())
    {
        printf("Failed!\n");
    }
    
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    /*glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);*/
    this->window = glfwCreateWindow(width, height, title.c_str(), NULL, NULL);
    if (this->window == nullptr)
    {
        glfwGetError(nullptr);
        exit(1);
        /*const char* description;
        int code = glfwGetError(&description);
        if (code != GLFW_NO_ERROR)
        {
            printf("Error code: %i\n", code);
            printf("%s\n", description);
            exit(1);
        }*/
    }
    glfwSetWindowUserPointer(window, (void*)this);
    if (!window)
    {
        printf("Window failed\n");
        // Window or context creation failed
    }

    glfwMakeContextCurrent(window);

    GLenum err = glewInit();
    if (GLEW_OK != err)
    {
        //std::cerr << "Glew Error: " << glewGetErrorString(err) << std::endl;
        printf("GLEW error: %s\n", glewGetErrorString(err));
        glfwTerminate();
        return;
    }
    glDisable( GL_DEPTH_TEST ); CheckOpenGLError();

    glViewport( 0, 0, width, height ); CheckOpenGLError();

    glfwSwapInterval(1);

    glfwSetWindowSizeCallback(window, window_size_callback);
    
    glEnable(GL_BLEND);
    glEnable(GL_TEXTURE_2D);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //init_imgui();
}


//#if defined(LOKI_CONTEXT_GLFW)
GLFWwindow *GLFWWindow::get_raw() const { return this->window; }
//#endif

/*GLFWWindow::GLFWWindow(int width, int height, const std::string &title)
{
#if defined(LOKI_CONTEXT_GLFW)
    //this->init_window_glfw_glew(width, height, title);
#endif
    this->init_window(width, height, title);
    //this->set_window_size(Vec2i(width, height));

    //init_imgui();
}*/

void GLFWWindow::clear(const Color &bgcolor)
{
    glClearColor( bgcolor.r / 255.0f, bgcolor.g / 255.0f, bgcolor.b / 255.0, bgcolor.a / 255.0 );
    glClear(GL_COLOR_BUFFER_BIT);
}


bool GLFWWindow::should_close() const
{
    return glfwWindowShouldClose(this->window);
}


void GLFWWindow::display()
{
    /*draw_gui();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());*/
    
    assert(window);
    glfwSwapBuffers(window);
    glfwPollEvents();

    //new_gui_frame();
}


void GLFWWindow::set_window_title(const std::string &title)
{
    glfwSetWindowTitle(this->window, title.c_str());
}
void GLFWWindow::set_window_size(const Vec2i &size)
{
    glfwSetWindowSize(this->window, size.x, size.y);
}

Vec2i GLFWWindow::get_window_size() 
{
    int x, y;
    glfwGetWindowSize(this->window, &x, &y);
    return Vec2i(x, y);
}

}