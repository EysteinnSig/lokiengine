#if defined(_WIN32) || defined(_WIN64)
#define LOKI_WINDOWS
#elif defined(__linux__)
#define LOKI_LINUX
#elif defined(__ANDROID__)
#define LOKI_ANDROID
#else
#error Platform not supported
#endif
