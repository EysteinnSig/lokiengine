#include <iostream>
#include "graphics/opengl.h"
#include "graphics/window.h"
#include "graphics/glfw_window.h"
#include "misc/system.h"
#include "bitmap/bitmap.h"
#include "examples/color_triangle.h"
#include "examples/texture_triangle.h"
#include "game.h"

int main(int, char**)
{
    //example_color_triangle();
    //example_texture_triangle();
    game();
}